#include <iostream>
#include <windows.h>

#if defined(__APPLE__) || defined(__MACOSX)
#include <OpenCL/cl.hpp>
#else
#include <CL/cl.h>
#endif

#pragma comment(lib, "OpenCL.lib")

typedef cl_int(__stdcall* GetPlatformIDs)(cl_uint,cl_platform_id *,cl_uint *);
typedef cl_int (__stdcall *GetPlatformInfo)(cl_platform_id platform,cl_platform_info param_name,size_t param_value_size,void *param_value,size_t *param_value_size_ret);
typedef cl_context (__stdcall *CreateContextFromType)(const cl_context_properties *properties,cl_device_type device_type,void (CL_CALLBACK *pfn_notify)(const char *errinfo,const void *private_info,size_t cb,void *user_data),void *user_data,cl_int *errcode_ret);
typedef cl_int (__stdcall *GetContextInfo)(cl_context context,cl_context_info param_name,size_t param_value_size,void *param_value,size_t *param_value_size_ret);
typedef cl_program (__stdcall *CreateProgramWithSource)(cl_context context,cl_uint count,const char ** strings,const size_t *lengths,cl_int *errcode_ret);
typedef cl_int (__stdcall *BuildProgram)(cl_program program,cl_uint num_devices,const cl_device_id *device_list,const char *options,void (*pfn_notify)(cl_program, void *user_data),void *user_data);                         
typedef cl_kernel (__stdcall *CreateKernel) (cl_program program,const char *kernel_name,cl_int *errcode_ret);                         
typedef cl_command_queue (__stdcall *CreateCommandQueue)(cl_context context,cl_device_id device,cl_command_queue_properties properties,cl_int *errcode_ret);                       
typedef cl_mem (__stdcall *CreateBuffer)(cl_context context,cl_mem_flags flags,size_t size,void *host_ptr,cl_int *errcode_ret);
typedef cl_int (__stdcall *SetKernelArg)(cl_kernel kernel,cl_uint arg_index,size_t arg_size,const void *arg_value);
typedef cl_int (__stdcall *EnqueueNDRangeKernel)(cl_command_queue command_queue,cl_kernel kernel,cl_uint work_dim,const size_t *global_work_offset,const size_t *global_work_size,const size_t *local_work_size,cl_uint num_events_in_wait_list,const cl_event *event_wait_list,cl_event *event);
typedef cl_int (__stdcall *Finish)(cl_command_queue command_queue );
typedef cl_int (__stdcall *EnqueueReadBuffer)(cl_command_queue command_queue,cl_mem buffer,cl_bool blocking_read,size_t offset,size_t cb,void *ptr,cl_uint num_events_in_wait_list,const cl_event *event_wait_list,cl_event *event);
typedef cl_int (__stdcall *ReleaseKernel)(cl_kernel kernel); 
typedef cl_int (__stdcall *ReleaseProgram)(cl_program program);
typedef cl_int (__stdcall *ReleaseMemObject)(cl_mem memobj);
typedef cl_int (__stdcall *ReleaseCommandQueue)(cl_command_queue command_queue); 
typedef cl_int (__stdcall *ReleaseContext)(cl_context context);

#define KERNEL(...)#__VA_ARGS__
const char *
kernelSourceCode = KERNEL(

__kernel void hellocl(__global uint *buffer)
{
	size_t gidx = get_global_id(0);
	size_t gidy = get_global_id(1);
	size_t lidx = get_local_id(0);
	buffer[gidx + 4 * gidy] = (1<<gidx)|(0x10<<gidy);
});

int main()
{
	printf("hello OpenCL\n");
	cl_int status = 0;
	size_t deviceListSize;

	cl_uint numPlatforms;
	cl_platform_id platform = NULL;

	GetPlatformIDs myGetPlatformIDs;
	GetPlatformInfo myGetPlatformInfo;
	CreateContextFromType myCreateContextFromType;
	GetContextInfo myGetContextInfo;
	CreateProgramWithSource myCreateProgramWithSource;
	BuildProgram myBuildProgram;
	CreateKernel myCreateKernel;
	CreateCommandQueue myCreateCommandQueue;
	CreateBuffer myCreateBuffer;
	SetKernelArg mySetKernelArg;
	EnqueueNDRangeKernel myEnqueueNDRangeKernel;
	Finish myFinish;
	EnqueueReadBuffer myEnqueueReadBuffer;
	ReleaseKernel myReleaseKernel;
	ReleaseProgram myReleaseProgram;
	ReleaseMemObject myReleaseMemObject;
	ReleaseCommandQueue myReleaseCommandQueue;
	ReleaseContext myReleaseContext;

	 HMODULE hInst;

	hInst = LoadLibrary("OpenCL.dll");
	if(!hInst)
	{
		printf("����OpenCL.dllʧ��");
		return -1;
	}

	myGetPlatformIDs = (GetPlatformIDs)GetProcAddress(hInst,"clGetPlatformIDs");
	myGetPlatformInfo = (GetPlatformInfo)GetProcAddress(hInst,"clGetPlatformInfo");
	myCreateContextFromType = (CreateContextFromType)GetProcAddress(hInst,"clCreateContextFromType");
	myGetContextInfo = (GetContextInfo)GetProcAddress(hInst,"clGetContextInfo");
	myCreateProgramWithSource = (CreateProgramWithSource)GetProcAddress(hInst,"clCreateProgramWithSource");
	myBuildProgram = (BuildProgram)GetProcAddress(hInst,"clBuildProgram");
	myCreateKernel = (CreateKernel)GetProcAddress(hInst,"clCreateKernel");
	myCreateCommandQueue = (CreateCommandQueue)GetProcAddress(hInst,"clCreateCommandQueue");
	myCreateBuffer = (CreateBuffer)GetProcAddress(hInst,"clCreateBuffer");
	mySetKernelArg = (SetKernelArg)GetProcAddress(hInst,"clSetKernelArg");
	myEnqueueNDRangeKernel = (EnqueueNDRangeKernel)GetProcAddress(hInst,"clEnqueueNDRangeKernel");
	myFinish = (Finish)GetProcAddress(hInst,"clFinish");
	myEnqueueReadBuffer = (EnqueueReadBuffer)GetProcAddress(hInst,"clEnqueueReadBuffer");
	myReleaseKernel = (ReleaseKernel)GetProcAddress(hInst,"clReleaseKernel");
	myReleaseProgram = (ReleaseProgram)GetProcAddress(hInst,"clReleaseProgram");
	myReleaseMemObject = (ReleaseMemObject)GetProcAddress(hInst,"clReleaseMemObject");
	myReleaseCommandQueue = (ReleaseCommandQueue)GetProcAddress(hInst,"clReleaseCommandQueue");
	myReleaseContext = (ReleaseContext)GetProcAddress(hInst,"clReleaseContext");

	if(!myGetPlatformIDs)
	{
		printf("��ȡclGetPlatformIDs������ַʧ�ܣ�");
		return -1;
	}

	status = myGetPlatformIDs(0, NULL, &numPlatforms);
	if(status != CL_SUCCESS)
	{
		printf("Error: Getting Platforms.\
			   (clGetPlatformsIDs)\n");
		return EXIT_FAILURE;
	}

	if(numPlatforms > 0)
	{
		cl_platform_id *platforms = (cl_platform_id*)malloc(numPlatforms*sizeof(cl_platform_id));
		status = myGetPlatformIDs(numPlatforms, platforms, NULL);
		if(status != CL_SUCCESS)
		{
			printf("Error: Getting Platform Ids.\
				   (clGetPlatformsIDs)\n");
			return -1;
		}
		for(unsigned int i = 0; i < numPlatforms; ++i)
		{
			char pbuff[100];
			status = myGetPlatformInfo(
				platforms[i],
				CL_PLATFORM_VENDOR,
				sizeof(pbuff),
				pbuff,
				NULL);
			platform = platforms[i];
			if(!strcmp(pbuff, "Advanced Micro Devices,Inc."))
			{
				break;
			}
		}
		delete platforms;
	}


	cl_context_properties cps[3] = {
		CL_CONTEXT_PLATFORM,
		(cl_context_properties)platform, 0};
		cl_context_properties *cprops = (NULL == platform) ? NULL : cps;


		cl_context context = myCreateContextFromType(
			cprops,
			CL_DEVICE_TYPE_CPU,
			NULL,
			NULL,
			&status);
		if(status != CL_SUCCESS)
		{
			printf("Error: Creating Context.\
				   (clCreateContextFromType)\n");
			return EXIT_FAILURE;
		}



		status = myGetContextInfo(context,
			CL_CONTEXT_DEVICES,
			0,
			NULL,
			&deviceListSize);
		if(status != CL_SUCCESS)
		{
			printf("Error: Getting Context Info \
				   (device list size,clGetContextInfo)\n");
			return EXIT_FAILURE;
		}
		cl_device_id *devices = (cl_device_id *)malloc(deviceListSize);
		if(devices == 0)
		{
			printf("Error: No devices found.\n");
			return EXIT_FAILURE;
		}


		status = myGetContextInfo(
			context,
			CL_CONTEXT_DEVICES,
			deviceListSize,
			devices,
			NULL);
		if(status != CL_SUCCESS)
		{
			printf("Error: Getting Context Info \
				   (device list, clGetContextInfo)\n");
			return EXIT_FAILURE;
		}


		size_t sourceSize[] = {strlen(kernelSourceCode)};
		cl_program program = myCreateProgramWithSource(
			context,
			1,
			&kernelSourceCode,
			sourceSize,
			&status);
		if(status != CL_SUCCESS)
		{
			printf("Error: Loading Binary into cl_program \
				   (clCreateProgramWithBinary)\n");
			return EXIT_FAILURE;
		}


		status = myBuildProgram(program, 1, devices, NULL, NULL, NULL);
		if(status != CL_SUCCESS)
		{
			printf("Error: Building Program \
				   (clBuildProgram)\n");
			return EXIT_FAILURE;
		}

		cl_kernel kernel = myCreateKernel(program, "hellocl", &status);
		if(status != CL_SUCCESS)
		{
			printf("Error: Creating Kernel from program.\
				   (clCreateKernel)\n");
			return EXIT_FAILURE;
		}


		cl_command_queue commandQueue = myCreateCommandQueue(context, devices[0], 0, &status);
		if(status != CL_SUCCESS)
		{
			printf("Creating Command Queue.\
				   (clCreatCommandQueue)\n");
			return EXIT_FAILURE;
		}


		unsigned int *outbuffer = new unsigned int [4 * 4];
		memset(outbuffer, 0, 4 * 4 * 4);
		cl_mem outputBuffer = myCreateBuffer(
			context,
			CL_MEM_ALLOC_HOST_PTR,
			4 * 4 * 4,
			NULL,
			&status);
		if(status != CL_SUCCESS)
		{
			printf("Error: clCreateBuffer \
				   (outputBuffer)\n");
			return EXIT_FAILURE;
		}

		status = mySetKernelArg(kernel, 0, sizeof(cl_mem), (void*)&outputBuffer);
		if(status != CL_SUCCESS)
		{
			printf("Error: Setting kernel argument.\
				   (output)\n");
			return EXIT_FAILURE;
		}

		size_t globalThreads[] = {4, 4};
		size_t localThreads[] = {2, 2};
		status = myEnqueueNDRangeKernel(
			commandQueue, kernel, 2, NULL, globalThreads,
			localThreads, 0, NULL, NULL);
		if(status != CL_SUCCESS)
		{
			printf("Error: Enqueueing kernel\n");
			return EXIT_FAILURE;
		}
		status = myFinish(commandQueue);
		if(status != CL_SUCCESS)
		{
			printf("Error: Finish command queue\n");
			return EXIT_FAILURE;
		}
		status = myEnqueueReadBuffer(commandQueue, outputBuffer, CL_TRUE, 0, 4 * 4 * 4, outbuffer, 0, NULL, NULL);
		if(status != CL_SUCCESS)
		{
			printf("Error: Read Buffer queue\n");
			return EXIT_FAILURE;
		}
		printf("out:\n");
		for(int i = 0; i < 16; i++)
		{
			printf("%x ", outbuffer[i]);
			if((i + 1) % 4 == 0)
			{
				printf("\n");
			}
		}

		status = myReleaseKernel(kernel);
		status = myReleaseProgram(program);
		status = myReleaseMemObject(outputBuffer);
		status = myReleaseCommandQueue(commandQueue);
		status = myReleaseContext(context);
		free(devices);
		delete outbuffer;
		::FreeLibrary(hInst);//�ͷ�Dll����
		return 0;
}

